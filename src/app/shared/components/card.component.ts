import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <p class="title">
      card works! {{counter}}
    </p>
    
    <li *ngFor="let item of lista">
      {{item}}
    </li>
  `,
  styles: [`
    .title {
      
    }
  `]
})
export class CardComponent {
  @Input() counter = 0;
  @Input() lista: any[] = []

  render() {
    console.log('card render')
  }
}
