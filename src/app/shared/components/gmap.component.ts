import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { google } from 'google-maps';

@Component({
  selector: 'app-gmap',
  template: `
    <div #host class="map"></div>
  `,
  styles: [`
    .map {
      height: 200px;
    }
  `]

})
export class GmapComponent  implements AfterViewInit {
  @ViewChild('host') host!: ElementRef<HTMLDivElement>;
  @Input() coords: any;
  @Input() zoom: number = 5;
  @Output() onDrag = new EventEmitter();

  map!: google.maps.Map<Element>
  marker!: google.maps.Marker

  ngOnChanges(changes: SimpleChanges) {
    if (this.map) {
      if (changes['zoom']) {
        this.map.setZoom(this.zoom)
      }
      if (changes['coords']) {
        this.map.setCenter(this.coords)

        this.marker.setPosition(this.coords)
      }
    }
  }

  ngAfterViewInit(): void {
    this.map = new google.maps.Map(this.host.nativeElement as HTMLElement, {
      center: this.coords,
      zoom: this.zoom,
    });
    this.marker = new google.maps.Marker({
      position: this.coords,
      draggable: true,
      map: this.map as any,
      title: "Hello World!",
    });
    this.marker.addListener("dragstart", () => {
      console.log('dragstart')
    });
    this.marker.addListener("dragend", (event) => {
      this.onDrag.emit({
        lat: event.latLng.lat(),
        lng: event.latLng.lng()
      });
    });

  }


}
