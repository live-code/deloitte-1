import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { CardComponent } from './components/card.component';
import { CardModule } from './components/card.module';
import { PanelComponent } from './components/panel.component';
import { ErrorMinLengthBarComponent } from './forms/error-min-length-bar.component';
import { GetPercPipe } from './forms/get-perc.pipe';
import { ErrorsMsgComponent } from './forms/errors-msg.component';
import { ColorPickerComponent } from './forms/color-picker/color-picker.component';
import { RatingComponent } from './forms/rating.component';
import { MyInputComponent } from './forms/my-input.component';
import { MyInput2Component } from './forms/my-input2.component';
import { MyFormInfoComponent } from './forms/my-form-info.component';
import { GmapComponent } from './components/gmap.component';
import { GmapComponent as GMapForm } from './forms/gmap.component';



@NgModule({
  declarations: [
    PanelComponent,
    ErrorMinLengthBarComponent,
    GetPercPipe,
    ColorPickerComponent,
    RatingComponent,
    MyInputComponent,
    MyInput2Component,
    MyFormInfoComponent,
    GmapComponent,
    GMapForm,
  ],
  imports: [
    CommonModule,
    CardModule,
    ReactiveFormsModule,
    ErrorsMsgComponent
  ],
  exports: [
    PanelComponent,
    CardModule,
    ErrorsMsgComponent,
    ErrorMinLengthBarComponent,
    ColorPickerComponent,
    RatingComponent,
    MyInputComponent,
    MyInput2Component,
    MyFormInfoComponent,
    GmapComponent,
    GMapForm,
  ]
})
export class SharedModule { }
