import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { google } from 'google-maps';

@Component({
  selector: 'app-input-gmap',
  template: `
    <div #host class="map"></div>
  `,
  styles: [`
    .map {
      height: 200px;
    }
  `],
  providers: [
    { provide: NG_VALUE_ACCESSOR, multi: true, useExisting: GmapComponent}
  ]
})
export class GmapComponent  implements ControlValueAccessor {
  @ViewChild('host', { static: true }) host!: ElementRef<HTMLDivElement>;
  @Input() zoom: number = 5;
  onChange!: (obj: any) => void
  onTouched!: () => void

  map!: google.maps.Map<Element>
  marker!: google.maps.Marker;

  ngOnInit(): void {
    this.map = new google.maps.Map(this.host.nativeElement as HTMLElement, {
      zoom: this.zoom,
    });

    this.marker = new google.maps.Marker({
      // position: this.coords,
      draggable: true,
      map: this.map as any,
      title: "Hello World!",
    });
    this.marker.addListener("dragstart", () => {
      this.onTouched();
    });
    this.marker.addListener("dragend", (event) => {
      this.onChange({
        lat: event.latLng.lat(),
        lng: event.latLng.lng()
      })
    });

  }

  writeValue(obj: any): void {
    this.map.setCenter(obj)
    this.marker.setPosition(obj)
  }
  registerOnChange(fn: any): void {
    this.onChange = fn
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.map) {
      if (changes['zoom']) {
        this.map.setZoom(this.zoom)
      }
    }
  }
}
