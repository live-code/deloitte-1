import { Component, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-rating',
  template: `
    
    <div style="display: flex">
      <i 
        *ngFor="let star of [null, null, null, null, null]; let i = index"
        class="fa"
        (click)="onClickHandler(i + 1)"
        [ngClass]="{
          'fa-star': i <= value - 1,
          'fa-star-o': i > value - 1
        }"
      ></i>
    </div>
  `,
  providers: [
    { provide: NG_VALUE_ACCESSOR, multi: true, useExisting: RatingComponent }
  ]
})
export class RatingComponent implements ControlValueAccessor{
  @Input() totalStars = 5;
  value: number = 0;

  onChange!: (newRate: number) => void
  onTouched!: () => void

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(value: number): void {
    this.value = value;
  }

  onClickHandler(rate: number) {
    this.value = rate;
    this.onChange(rate);
    this.onTouched()
  }
}
