import { ChangeDetectorRef, Component, forwardRef } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormBuilder,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR, ValidationErrors,
  Validator,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-my-form-info',
  template: `
    
    <div *ngIf="form.invalid">form errors</div>
    <div [formGroup]="form">
      <input 
        [style.background-color]="form.get('address')?.invalid ? 'red' : null"
        formControlName="address" placeholder="address">
      <input type="text" formControlName="city"  placeholder="city"/>
    </div>
  `,
  providers: [
    { provide: NG_VALUE_ACCESSOR, multi: true, useExisting: forwardRef(() => MyFormInfoComponent) },
    { provide: NG_VALIDATORS, multi: true, useExisting: forwardRef(() => MyFormInfoComponent) },
  ]
})
export class MyFormInfoComponent implements ControlValueAccessor, Validator {
  onTouched!: () => void;

  form = this.fb.group({
    address: ['', [Validators.required, Validators.minLength(3)]],
    city: ['', Validators.required],
  })


  constructor(private fb: FormBuilder, private cd: ChangeDetectorRef) {
  }

  registerOnChange(fn: any): void {
    this.form.valueChanges.subscribe(fn)
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(obj: any) {
    this.form.patchValue(obj)
  }

  validate(c: AbstractControl): ValidationErrors | null {

    return c &&  this.form.invalid
      ? { infoInvalidFields: true }
      : null;
  }
}


//    constructor(@Self() @Optional() private control: NgControl) {
