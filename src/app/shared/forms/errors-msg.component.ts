import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-errors-msg',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div *ngIf="errors as err">
      <div *ngIf="err['required']">required</div>
      <div *ngIf="err['minlength']">
        missing {{err['minlength']?.['requiredLength'] - err['minlength']?.['actualLength']}} chars
      </div>
    </div>
  `,
  styles: [
  ]
})
export class ErrorsMsgComponent {
  @Input() errors: ValidationErrors | null = null;
}
