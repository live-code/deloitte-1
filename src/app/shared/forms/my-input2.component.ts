import { Component, Injector, Input } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR, NgControl, ValidationErrors,
  Validator,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-my-input2',
  template: `
    <div style="margin: 10px 0 ">
      <div *ngIf="ngControl.errors?.['required'] ">this field is required</div>
      <div *ngIf="ngControl.errors?.['minlength'] ">this field is too short</div>
      <div *ngIf="ngControl.errors?.['maxlength'] ">this field is too long</div>
      <div *ngIf="ngControl.errors?.['alphaNumeric'] ">no symbols allowed</div>

      <div>
        <label for="Your Name">{{label}}</label>
      </div>
      <input
        [formControl]="input"
        type="text" [placeholder]="label" 
        (blur)="onTouched()"
      >
    </div>
  `,
  providers: [
    { provide: NG_VALUE_ACCESSOR, multi: true, useExisting: MyInput2Component },
    { provide: NG_VALIDATORS, multi: true, useExisting: MyInput2Component },
  ]
})
export class MyInput2Component implements ControlValueAccessor, Validator{
  @Input() label = ''
  @Input() alphaNumeric: boolean = false;
  // @Input() required: boolean = false;
  input = new FormControl('');
  ngControl!: NgControl;


    constructor(private inj: Injector) {
  }

  ngOnInit() {
    this.ngControl =  this.inj.get(NgControl)
    console.log(this.ngControl)
  }

  onTouched!: () => void

  registerOnChange(fn: any): void {
    this.input.valueChanges.subscribe(fn)
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(text: string): void {
    this.input.setValue(text)
  }

  validate(control: AbstractControl): ValidationErrors | null {
    if (
      control.value &&
      this.alphaNumeric &&
      !control.value.match(ALPHA_NUMERIC_REGEX)
    ) {
      return { alphaNumeric: true }
    }
    return null;
  }


  setDisabledState?(isDisabled: boolean): void {
    if (isDisabled) {
      this.input.disable();
    } else {
      this.input.enable();
    }
  }

}

// custom validator
export const ALPHA_NUMERIC_REGEX = /^\w+$/
