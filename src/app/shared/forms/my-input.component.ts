import { Component, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-my-input',
  template: `
    <div>
      <div>
        <label for="Your Name">{{label}}</label>
      </div>
      <input
        type="text" [placeholder]="label" [value]="text" (blur)="onTouched()"
        (input)="onChangeHandler($event)"
      >
    </div>
  `,
  providers: [
    { provide: NG_VALUE_ACCESSOR, multi: true, useExisting: MyInputComponent }
  ]
})
export class MyInputComponent implements ControlValueAccessor{
  @Input() label = ''
  text: string = '';

  onChange!: (newText: string) => void
  onTouched!: () => void

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(text: string): void {
    this.text = text;
  }


  onChangeHandler(event: Event) {
    const val = (event.target as HTMLInputElement).value;
    this.onChange(val);
  }
}
