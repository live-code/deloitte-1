import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-color-picker',
  template: `
      <div style="display: flex; gap: 3px">
        
        <div 
          *ngFor="let c of colors"
          class="cell"
          (click)="clickHandler(c)"
          [ngClass]="{active: c === color}"
          [style.background-color]="c"
          [style.opacity]="disabled ? 0.4 : 1"
          [style.pointer-events]="disabled ? 'none' : null"
        >
        </div>
      </div>
  `,
  providers: [
    { provide: NG_VALUE_ACCESSOR, multi: true, useExisting: ColorPickerComponent }
  ],
  styles: [`
    .cell {
      width: 20px;
      height: 20px;
      background-color: gray;
    }
    .active {
      border: 2px solid black;
    }
  `]
})
export class ColorPickerComponent implements ControlValueAccessor {
  @Input() colors = [
    '#F44336', '#90CAF9', '#FFCDD2', '#69F0AE', '#AED581', '#FFE082'
  ];
  color: string = '';
  disabled: boolean = false;

  onChange!: (newColor: string) => void
  onTouched!: () => void

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  writeValue(color: string): void {
    this.color = color
  }

  clickHandler(c: string) {
    this.color = c;
    this.onChange(c);
    this.onTouched();
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
    this.color = '';
  }
}
