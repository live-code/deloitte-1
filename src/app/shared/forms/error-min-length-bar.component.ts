import { Component, Input, ViewEncapsulation } from '@angular/core';
import { ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-error-min-length-bar',
  template: `
    <div
      class="bar"
      *ngIf="errors"
      [style.width.%]="errors | getPerc"
    ></div>
  `,
  styles: [`
    .bar {
      width: 100%;
      height: 5px;
      margin-top: 2px;
      background-color: green;
      border-radius: 10px;
      transition: width 1s cubic-bezier(0.68, -0.6, 0.32, 1.6);
    }
  `]
})
export class ErrorMinLengthBarComponent {
  @Input() errors!: ValidationErrors;
}
