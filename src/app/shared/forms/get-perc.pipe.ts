import { Pipe, PipeTransform } from '@angular/core';
import { ValidationErrors } from '@angular/forms';

@Pipe({
  name: 'getPerc'
})
export class GetPercPipe implements PipeTransform {
  transform(errors: ValidationErrors): number {
    const err = errors['minlength']
    return err ? (err.actualLength / err.requiredLength) * 100 : 0;
  }

}
