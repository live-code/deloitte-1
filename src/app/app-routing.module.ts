import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule )
  },
  { path: 'catalog', loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule) },
  { path: 'contacts', loadChildren: () => import('./features/contacts/contacts.module').then(m => m.ContactsModule) },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'crud', loadChildren: () => import('./features/crud/crud.module').then(m => m.CrudModule) },
  { path: 'demo1', loadComponent: () => import('./features/demo1/demo1.component')},
  { path: 'cms', loadChildren: () => import('./features/cms/cms.module').then(m => m.CmsModule) },
  { path: 'form2', loadChildren: () => import('./features/form2/form2.module').then(m => m.Form2Module) },
  { path: 'form3', loadChildren: () => import('./features/form3/form3.module').then(m => m.Form3Module) },
  { path: 'form4', loadChildren: () => import('./features/form4/form4.module').then(m => m.Form4Module) },
  { path: 'form5', loadChildren: () => import('./features/form5/form5.module').then(m => m.Form5Module) }
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule {}
