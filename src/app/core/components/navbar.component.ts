import { Component } from '@angular/core';

@Component({
  selector: 'app-navbar',
  template: `
    <button routerLink="home" routerLinkActive="active">home</button>
    <button [routerLink]="'catalog'" routerLinkActive="active">catalog</button>
    <button routerLink="demo1" routerLinkActive="active">demo1</button>
    <button routerLink="crud" routerLinkActive="active">crud</button>
    <button routerLink="cms" routerLinkActive="active">cms</button>
    <button routerLink="form2" routerLinkActive="active">form2</button>
    <button routerLink="form3" routerLinkActive="active">form3</button>
    <button routerLink="form4" routerLinkActive="active">form4</button>
    <button routerLink="form5" routerLinkActive="active">form5 - array</button>
    <button routerLink="contacts" routerLinkActive="active">contacts</button>
  `,
  styles: [`
    .active {
      background-color: orange;
    }
  `]
})
export class NavbarComponent {
}
