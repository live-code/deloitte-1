import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <p>
      home works!
    </p>
    
    
    <app-news></app-news>
    <app-gallery></app-gallery>
  `,
  styles: [
  ]
})
export class HomeComponent {

}
