import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { NewsComponent } from './components/news.component';
import { GalleryComponent } from './components/gallery.component';

@NgModule({
  declarations: [
    HomeComponent,
    NewsComponent,
    GalleryComponent,
    // ....
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: HomeComponent}
    ])
  ]
})
export class HomeModule { }
