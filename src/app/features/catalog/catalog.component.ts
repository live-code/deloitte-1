import { Component } from '@angular/core';

@Component({
  selector: 'app-catalog',
  template: `
    <button routerLink="one" routerLinkActive="active">one</button>
    <button routerLink="two" routerLinkActive="active">two</button>
    <button routerLink="three" routerLinkActive="active">three</button>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: [`
    .active {
      background-color: purple;
      color: white;
    }
  `]
})
export class CatalogComponent {

}
