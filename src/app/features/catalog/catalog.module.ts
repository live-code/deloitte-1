import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { CatalogComponent } from './catalog.component';
import { OneComponent } from './one/one.component';
import { TwoComponent } from './two/two.component';
import { DetailsComponent } from './details/details.component';

@NgModule({
  declarations: [
    CatalogComponent,
    OneComponent,
    TwoComponent,
    DetailsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: CatalogComponent,
        children: [

          { path: 'one', component: OneComponent }, // eager
          { path: 'two', component: TwoComponent }, // eager
          { path: 'three', loadChildren: () => import('./three/three.module').then(m => m.ThreeModule) }, // lazy
          { path: ':id', component: DetailsComponent }, // eager
          { path: '', redirectTo: 'one', pathMatch: 'full' }
        ]
      },
    ]),
    FormsModule
  ]
})
export class CatalogModule { }
