import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, mergeMap } from 'rxjs';

interface User {
  id: number;
  name: string;
  username: string;
}

@Component({
  selector: 'app-details',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <p>
      details works!
    </p>
    
    <div *ngIf="data$ | async as dt">
      ----
      {{ dt.id }}
      {{ dt.name }}
      ----
    </div>
    
    <button routerLink="../2">Next</button>
    
    <input type="text" ngModel>
    {{(data$ | async)?.username }}
    
    {{render()}}
  `,
})
export class DetailsComponent {
  data$ = this.activateRoute.params
    .pipe(
      map(params => params['id']),
      mergeMap(id => this.http.get<User>(`https://jsonplaceholder.typicode.com/users/${id}`)),
      // share()
    )

  constructor(
    private activateRoute: ActivatedRoute,
    private http: HttpClient
  ) {

  }

  render() {
    console.log('render')
  }
}
