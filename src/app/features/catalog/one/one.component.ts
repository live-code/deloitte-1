import { Component } from '@angular/core';

@Component({
  selector: 'app-one',
  template: `
    <p>
      one works!
    </p>
    
     <button routerLink="../1">1</button>
     <button routerLink="../2">2</button>
     <button routerLink="../3">3</button>
    
  `,
  styles: [
  ]
})
export class OneComponent {

}
