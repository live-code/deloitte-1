import { AfterViewInit, Component } from '@angular/core';

import { google } from 'google-maps';

@Component({
  selector: 'app-form4',
  template: `
    <p>
      form4 works!
    </p>
    <app-gmap
      [coords]="coords"
      [zoom]="zoom" 
      (onDrag)="printData($event)"
    ></app-gmap>
    
    <button (click)="coords =  { lat: 55, lng: 12 }">Trieste</button>
    <button (click)="coords =  { lat: 33, lng: 13 }">Palermo</button>
    {{zoom}}
    <button (click)="zoom = zoom -1">-</button>
    <button (click)="zoom = zoom +1">+</button>
    
  `,
})
export class Form4Component {
  zoom = 10;
  coords: any = { lat: 43, lng: 13 }

  printData($event: any) {
    console.log($event)
  }
}

