import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ErrorsMsgComponent } from '../../shared/forms/errors-msg.component';
import { SharedModule } from '../../shared/shared.module';

import { CrudRoutingModule } from './crud-routing.module';
import { CrudComponent } from './crud.component';


@NgModule({
  declarations: [
    CrudComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    CrudRoutingModule
  ]
})
export class CrudModule { }
