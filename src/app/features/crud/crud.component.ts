import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, ViewEncapsulation } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-crud',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div style="width: 150px">
      <app-errors-msg [errors]="myInput.errors"  />
      <input type="text" [formControl]="myInput">
    
      <app-error-min-length-bar
        *ngIf="myInput.errors"
        [errors]="myInput.errors"
      ></app-error-min-length-bar>
     
    </div>
     <div style="width: 150px">
       <app-errors-msg [errors]="myInput2.errors" />
       <input type="text" [formControl]="myInput2">
    
       <app-error-min-length-bar
        *ngIf="myInput2.errors"
        [errors]="myInput2.errors"
       ></app-error-min-length-bar>
    </div>
    
  `,

})
export class CrudComponent  {
  // myInput = new FormControl('', { nonNullable: true });
  myInput = new FormControl('', [ Validators.required, Validators.minLength(5) ]);
  myInput2 = new FormControl('', [ Validators.required, Validators.minLength(3) ]);

}
