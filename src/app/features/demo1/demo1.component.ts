import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-demo1',
  standalone: true,
  imports: [CommonModule],
  template: `
    <p>
      demo1 works!
    </p>
  `,
  styles: [
  ]
})
export default class Demo1Component {

}
