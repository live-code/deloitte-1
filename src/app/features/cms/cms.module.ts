import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { CmsRoutingModule } from './cms-routing.module';
import { CmsComponent } from './cms.component';


@NgModule({
  declarations: [
    CmsComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CmsRoutingModule
  ]
})
export class CmsModule { }
