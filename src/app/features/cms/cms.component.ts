import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { debounceTime, mergeMap } from 'rxjs';

@Component({
  selector: 'app-cms',
  template: `
    <p>
      cms works!
    </p>
    
    <form [formGroup]="form" (submit)="save()">
      <div *ngIf="form.get('name')?.errors?.['required']">field required</div>
      
      <input type="checkbox" id="cmp" formControlName="isCompany">
      <label for="cmp">are you a company?</label>
      <hr>

      <div style="border: 4px solid green">
        <h1>
          <span>V</span>
          anagrafica
        </h1>
        <input type="text" formControlName="name" />
        <input type="text" formControlName="surname" />
      </div>
      <hr>
      <button type="submit" [disabled]="form.invalid">save</button>
    </form>
    
    <pre>name error:{{form.get('name')?.errors | json}}</pre>
    
  `,
})
export class CmsComponent {
  form = this.fb.nonNullable.group(
    {
      isCompany: false,
      name: ['', [Validators.required, alphaNumeric]],
      surname: [''],
    }
  )

  constructor(private fb: FormBuilder) {
    this.form.get('isCompany')?.valueChanges
      .subscribe(isCompany => {
        if(isCompany) {
          this.form.get('surname')?.setValidators(null)
        } else {
          this.form.get('surname')?.setValidators([Validators.required])
        }
        this.form.get('surname')?.updateValueAndValidity();
      })

    this.form.get('surname')?.updateValueAndValidity();

    this.form.get('isCompany')?.setValue(true)
  }

  save() {
  }
}

export const ALPHA_NUMERIC_REGEX = /^\w+$/

function alphaNumeric(c: FormControl): ValidationErrors | null {
  return (c.value && !c.value.match(ALPHA_NUMERIC_REGEX))
    ?  { alphaNumeric: true }
    : null;
}
