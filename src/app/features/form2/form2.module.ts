import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';

import { Form2RoutingModule } from './form2-routing.module';
import { Form2Component } from './form2.component';


@NgModule({
  declarations: [
    Form2Component
  ],
  imports: [
    CommonModule,
    Form2RoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class Form2Module { }
