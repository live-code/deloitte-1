import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { debounceTime, mergeMap } from 'rxjs';

@Component({
  selector: 'app-form2',
  template: `
    
    <form [formGroup]="form" (submit)="save()">

      <app-color-picker formControlName="color" ></app-color-picker>
      
      <div
        *ngIf="step === 1"
        [style.border]="form.get('anagrafica')?.valid ? '4px solid green' : null">
        <h1>
          <span *ngIf="form.get('anagrafica')?.valid">V</span>
          anagrafica
        </h1>
        
        <div formGroupName="anagrafica">
          <input type="text" formControlName="name" />
          <input type="text" formControlName="surname" />
        </div>
        
        <button
          *ngIf="form.get('anagrafica')?.valid"
          (click)="step = 2">Next</button>
      </div>



      <div *ngIf="step === 2">
        <h1>
          Details
        </h1>

        <div formGroupName="details">
          <input type="text" formControlName="cost" />
          <input type="text" formControlName="expired" />
        </div>
      </div>
      
      
      
      <hr>
      <button type="submit" [disabled]="form.invalid">save</button>
    </form>
    
    <pre>{{form.value | json}}</pre>
    
  `,
})
export class Form2Component {
  step = 1;

  form = this.fb.nonNullable.group(
    {
      color: 'red',
      name: ['', Validators.required],
      anagrafica: this.fb.group({
        name: ['', [Validators.required, alphaNumeric]],
        surname: ['', Validators.required],
      }),
      details: this.fb.group({
        cost: 0,
        expired: 0
      })
    }

  )

  constructor(private fb: FormBuilder) {
    setTimeout(() => {
      const res = {
        color: 'green',
        name: 'nutella',
        anagrafica: {
          name: 'fabio',
          surname: 'biondi'
        },
        details: {
          cost: 123
        },

      }

      this.form.patchValue(res)
    }, 2000)


  }

  save() {
  }
}

export const ALPHA_NUMERIC_REGEX = /^\w+$/

function alphaNumeric(c: FormControl): ValidationErrors | null {
  return (c.value && !c.value.match(ALPHA_NUMERIC_REGEX))
    ?  { alphaNumeric: true }
    : null;
}
