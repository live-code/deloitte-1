import { Component } from '@angular/core';

@Component({
  selector: 'app-contacts',
  template: `
    <p>
      contacts works! {{name}}
    </p>
    
    <button (click)="count = count + 1">+</button>
    <button (click)="add()">add</button>
    
    {{list | json}}
    
    <app-card
      [lista]="list"
      [counter]="count"
    ></app-card>
  `,
  styles: [
  ]
})
export class ContactsComponent {
  count = 0;
  name = 'fabio';
  list: any[] = [];

  add() {
    // this.list.push(Math.random())
    this.list = [...this.list, Math.random()]

  }
}
