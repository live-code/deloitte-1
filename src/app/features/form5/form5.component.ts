import { Component } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form5',
  template: `
    <h1>Form array demo</h1>
    
    <form [formGroup]="form">
      <input type="text" formControlName="orderId" placeholder="order id">
      
      <div formGroupName="anagrafica">
        <input type="text" formControlName="name" placeholder="name">
        <input type="text" formControlName="surname" placeholder="surname">
      </div>
      <h3>Items</h3>
      <div formArrayName="items">
        <div 
          *ngFor="let item of items.controls; let i = index"
          [formGroupName]="i"
        >
          <i class="fa fa-check" *ngIf="item.valid"></i>
          <input type="text" placeholder="name" formControlName="name">
          <input type="text" placeholder="cost" formControlName="price">
          <i class="fa fa-plus-square"  *ngIf="item.valid" (click)="addItem()"></i>
          <i class="fa fa-trash"  (click)="removeItem(i)" *ngIf="items.controls.length > 1"></i>
          
          <h2>Tags</h2>
          <button (click)="addTag(i)">add tags</button>
          <div formArrayName="tags">
            <div *ngFor="let tag of getTags(item); let j = index"  [formGroupName]="j">
              <input type="text" formControlName="name">
            </div>
          </div>
          <hr>
          
        </div>
        
      </div>
      
     
    </form>
    
    <pre>{{form.value | json}}</pre>
  `,
})
export class Form5Component {
  form = this.fb.group({
    orderId: '',
    anagrafica: this.fb.group({
      name: '',
      surname: '',
    }),
    items: this.fb.array([]),
  })

  items = this.form.get('items') as FormArray

  constructor(private fb: FormBuilder) {
    this.addItem('', 0)
  }

  addItem(name: string = '', price: number = 0) {
    this.items.push(
      this.fb.group({
        name: this.fb.control(name, { validators: [Validators.required]}),
        price: this.fb.control(price, { validators: [Validators.required]}),
        tags: this.fb.array([])
      })
    )
  }

  removeItem(i: number) {
    this.items.removeAt(i)
  }

  addTag(index: number) {
    const tags = this.form.get('items')?.get(index.toString())?.get('tags') as FormArray;

    tags.push(
      this.fb.group({
        name: ''
      })
    )
  }

  getTags(item: AbstractControl) {
    return (item.get('tags') as FormArray).controls
  }

}
