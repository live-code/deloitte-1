import { Component, ViewChild } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators
} from '@angular/forms';
import { debounceTime, mergeMap } from 'rxjs';


@Component({
  selector: 'app-form3',
  template: `
    <form [formGroup]="form" (submit)="save()">
      <app-my-input2 formControlName="name" label="Your Name"  [alphaNumeric]="true"></app-my-input2>
      <app-color-picker formControlName="color" ></app-color-picker>
      <app-rating formControlName="rate"></app-rating>
      <app-my-form-info formControlName="info"></app-my-form-info>
      <app-input-gmap formControlName="position" [zoom]="5"></app-input-gmap>

      <div formGroupName="passwords">
        <h3>Passwords {{form.get('passwords')?.errors | json}}</h3>
        <h3>Passwords {{form.get('passwords')?.valid | json}}</h3>
        <input formControlName="password1" type="text" placeholder="Password" autocomplete="off">
        <input formControlName="password2" type="text" placeholder="Repeat Password" autocomplete="off">
      </div>
      
        <button type="submit" [disabled]="form.invalid">save</button>
    </form>
    
    <pre>dirty: {{form.dirty | json}}</pre>
    <pre>touched: {{form.touched | json}}</pre>
    <pre>{{form.value | json}}</pre>
    
  `,
})
export class Form3Component {

  form = this.fb.nonNullable.group(
    {
      name: ['', [Validators.required, alphaNumeric]],
      rate: 2,
      position: { lat: 43, lng: 13 },
      color: ['', Validators.required],
      info: null,
      passwords: this.fb.group(
        {
          password1: [''],
          password2: [''],
        },
        {
          validators: passwordMatch('password1', 'password2', 4)
        }
      )
    },
  )

  constructor(private fb: FormBuilder) {
    setTimeout(() => {
      this.form.get('position')?.setValue({  lat: 55, lng: 13 })
      // this.form.get('color')?.setValue('#69F0AE')
      // this.form.get('name')?.setValue('bbb')
      /*this.form.get('info')?.setValue({
        address: 'via x',
        city: 'Trieste'
      })*/
     /* this.form.get('color')?.disable();
      this.form.get('name')?.disable()
      this.form.get('info.address')?.setValue('bbb')*/
    }, 2000)


  }
  save() {}
}


export function passwordMatch(p1label: string, p2label: string, minlength = 3): ValidatorFn {
  return (g: AbstractControl) => {
    const p1 = g.get(p1label)?.value;
    const p2 = g.get(p2label)?.value;
    const isPassMatch =  (p1 === p2);
    const validPasswords = (p1.length > minlength) && (p2.length > minlength);

    return (isPassMatch && validPasswords) ? null : {
      passwords: {
        isPassMatch,
        validPasswords
      }
    }

  }
}


// custom validator
export const ALPHA_NUMERIC_REGEX = /^\w+$/

function alphaNumeric(c: FormControl): ValidationErrors | null {
  return (c.value && !c.value.match(ALPHA_NUMERIC_REGEX))
    ?  { alphaNumeric: true }
    : null;
}
